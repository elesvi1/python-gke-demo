FROM python:3.7.6

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src

# Install app dependencies
COPY requirements.txt /usr/src
RUN pip install -r requirements.txt

# Bundle app source
COPY . /usr/src/app
WORKDIR /usr/src/app

EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["/usr/src/app/app.py"]
