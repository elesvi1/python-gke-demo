pipelineJob('Build Python Demo App') {
    parameters {
        labelParam('swarm')
    }
          definition {
              cpsScm {
                  scriptPath 'Jenkinsfile'
                  scm {
                    git {
                        remote { 
                            url ('https://gitlab.com/elesvi1/python-gke-demo.git')
                            credentials ('gitlabuser')
                        }
                        branch '*/master'
                        extensions {}
                    }
                  }
              }
          }
      }
